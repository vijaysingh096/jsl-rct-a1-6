import React, { Component } from "react";
class Task1_aForm extends Component {
  state = {
    person: {
      name: "",
      age: "",
      country: "",
    },
    countries: ["United State of America", "Canada", "India", "United Kingdom"],
  };
  handleChange = (e) => {
    // let { currentTarget: input } = e;
    let s1 = { ...this.state };
    s1.person[e.currentTarget.name] = e.currentTarget.value;
    this.setState(s1);
  };
  handelSubmit = (e) => {
    e.preventDefault();
  };
  render() {
    let { person, countries } = this.state;
    let { name, age, country } = person;
    return (
      <React.Fragment>
        <div className="container">
          <div className="form-group">
            <label>Name</label>
            <input
              type="text"
              className="form-control"
              id="name"
              name="name"
              value={name}
              placeholder="Enter  Name "
              onChange={this.handleChange}
            />
          </div>

          <div className="form-group">
            <label>Age</label>
            <input
              type="text"
              className="form-control"
              id="age"
              name="age"
              value={age}
              placeholder="Enter  Age "
              onChange={this.handleChange}
            />
          </div>
          <div className="form-group">
            <label>Country</label>
            <select
              className="form-control"
              id="country"
              name="country"
              value={country}
              onChange={this.handleChange}
            >
              <option disabled value="">
                select the country
              </option>
              {countries.map((c1) => {
                return <option>{c1}</option>;
              })}
            </select>
          </div>

          <button className="btn btn-primary my-2" onClick={this.handelSubmit}>
            Submit
          </button>
        </div>
      </React.Fragment>
    );
  }
}
export default Task1_aForm;
