import React, { Component } from "react";
class LPForm3 extends Component {
  state = {};
  handleChange = (e) => {
    let s1 = { ...this.props.optionsSel };
    let { currentTarget: input } = e;
   
       (s1[input.name] = input.value);
    this.props.onChangeOption(s1);
  };


  render() {
    let { optionsArray, optionsSel } = this.props;
    return (
      <React.Fragment>
        <div className="container">
          {this.showRadioes(
            "Product Name",
            optionsArray.prodName,
            "prodName",
            optionsSel.prodName
          )}
        </div>
      </React.Fragment> 
    );
  }



  showRadioes = (label, arr, name, selVal) => {
    return (
      <React.Fragment>
        <label className="form-check-lable font-weight-bold">
          <b>{label} </b>
        </label>
        <br />
        {arr.map((r1) => (
          <div className="form-check ">
            <input
              className="form-check-input"
              type="radio"
              name={name}
              value={r1}
              checked={selVal === r1}
              onChange={this.handleChange}
            />
            <label className="form-check-lable">{r1}</label>
          </div>
        ))}
        <br />
      </React.Fragment>
    );
  };


//   showCheckBoxes = (label, arr, name, selArr) => {
//     return (
//       <React.Fragment>
//         <label className="form-check-lable">
//           <b>{label}</b>
//         </label>
//         {arr.map((opt) => (
//           <div className="form-check">
//             <input
//               className="form-check-input"
//               type="checkbox"
//               name={name}
//               value={opt}
//               checked={selArr.findIndex((sel) => sel === opt) >= 0}
//               onChange={this.handleChange}
//             />
//             <label className="form-check-lable">{opt}</label>
//           </div>
//         ))}
//         <br />
//       </React.Fragment>
//     );
//   };
}
export default LPForm3;
