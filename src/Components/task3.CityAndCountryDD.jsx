import React, { Component } from "react";
class CityAndCountryDD extends Component {
  state = {
    locs: [
      {
        country: "India",
        cities: ["New Delhi", "Mumbai", "Bangalore", "Chennai", "Pune"],
      },
      {
        country: "USA",
        cities: [
          "Los Angeles",
          "Chicago",
          "New York",
          "Seattle",
          "Washington DC",
        ],
      },
      { country: "France", cities: ["Paris", "Nice", "Lyon", "Cannes"] },
      { country: "Japan", cities: ["Tokyo", "Kyoto"] },
      { country: "China", cities: ["Shanghai", "Beijing", "Shenzen"] },
    ],

    selVal: {
      country1: "",
      city: "",
    },
  };

  handleChange = (e) => {
    let { currentTarget: input } = e;
    let s1 = { ...this.state };
    s1.selVal[input.name] = input.value;
    if (input.name === "country1") {
      s1.selVal.city = "";
    }
    this.setState(s1);
  };
  handelSubmit = (e) => {
    e.preventDefault();
    let s1 = { ...this.state };
    let {country1,city}=s1.selVal
    window.alert("Country "+ country1+" City "+ city);
  };

  render() {
    let { locs, selVal } = this.state;
    let { country1, city } = selVal;
    let Cities = country1
      ? locs.find((c1) => c1.country === country1).cities
      : [];
    return (
      <React.Fragment>
        <div className="container">
          <div className="form-group">
            <label>Country</label>
            <select
              className="form-control"
              id="country1"
              name="country1"
              value={country1}
              onChange={this.handleChange}
            >
              <option disabled value="">
                select the country
              </option>
              {locs.map((c1) => {
                return <option>{c1.country}</option>;
              })}
            </select>
          </div>
          {country1 ? (
            <div className="form-group">
              <label>City</label>
              <select
                className="form-control"
                id="city"
                name="city"
                value={city}
                onChange={this.handleChange}
              >
                <option disabled value="">
                  select the city
                </option>
                {Cities.map((c1) => {
                  return <option>{c1}</option>;
                })}
              </select>
            </div>
          ) : (
            ""
          )}
          <button className="btn btn-primary my-2" onClick={this.handelSubmit}>
            Submit
          </button>
        </div>
      </React.Fragment>
    );
  }
}
export default CityAndCountryDD;
