import React, { Component } from "react";
class InputFieldUsingCB extends Component {
  state = {
    selcVal: {
      working: "",
      studying: "",
      company: "",
      designation: "",
      college: "",
      course: "",
    },
  };

  handleChange = (e) => {
    let s1 = { ...this.state };
    let { currentTarget: input } = e;
    input.type === "checkbox"
      ? (s1.selcVal[input.name] = input.checked)
      : (s1.selcVal[input.name] = input.value);
    if (!s1.selcVal.working) {
      s1.selcVal.company = "";
      s1.selcVal.designation = "";
    }
    if (!s1.selcVal.studying) {
      s1.selcVal.college = "";
      s1.selcVal.course = "";
    }
    this.setState(s1);
    // console.log(this.state);
  };

  handelSubmit = (e) => {
    e.preventDefault();
    let { working, studying, company, designation, college, course } =
      this.state.selcVal;
    console.log(this.state.selcVal);
    window.alert("Working- " +working +", Company- "+ company +", Designation- "+ designation +", Studying- "+ studying +", College- "+ college +", Course- "+ course);
  };

  showTextField = (lable, type, id, name, val, placeholder) => {
    return (
      <div className="form-group">
        <label>{lable}</label>
        <input
          type={type}
          className="form-control"
          id={id}
          name={name}
          value={val}
          placeholder={placeholder}
          onChange={this.handleChange}
        />
      </div>
    );
  };

  render() {
    let { working, studying, company, designation, college, course } =
      this.state.selcVal;
    // console.log(this.state);
    return (
      <React.Fragment>
        <div className="container">
          <h3>Provide Details below</h3>
          <div className="form-check">
            <input
              className="form-check-input"
              type="checkbox"
              name="working"
              value="working"
              checked={working}
              onChange={this.handleChange}
            />
            <label className="form-check-lable">Working</label>
          </div>
          {working ? (
            <>
              <h4>Provide Job Details</h4>

              {this.showTextField(
                "Company Name",
                "text",
                "compName",
                "company",
                company,
                "Enter Company Name"
              )}

              {this.showTextField(
                "Designation",
                "text",
                "desig",
                "designation",
                designation,
                "Enter Designation"
              )}
            </>
          ) : (
            ""
          )}
          <div className="form-check">
            <input
              className="form-check-input"
              type="checkbox"
              name="studying"
              value="studying"
              checked={studying}
              onChange={this.handleChange}
            />
            <label className="form-check-lable">Studying</label>
          </div>
          {studying ? (
            <>
              <h4>Provide Cource Details</h4>

              {this.showTextField(
                "College Name",
                "text",
                "collegeName",
                "college",
                college,
                "Enter College Name"
              )}
              {this.showTextField(
                "Course",
                "text",
                "course",
                "course",
                course,
                "Enter Course name"
              )}
            </>
          ) : (
            ""
          )}
          <button className="btn btn-primary my-2" onClick={this.handelSubmit}>
            Submit
          </button>
        </div>
      </React.Fragment>
    );
  }
}
export default InputFieldUsingCB;
