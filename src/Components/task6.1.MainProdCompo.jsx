import React, { Component } from "react";
import LPForm1 from "./task6.1.LPForm";
class MainProdCompo1 extends Component {
  state = {
    products: [
      { id: "PEP110", name: "Pepsi", category: "Food", stock: true },
      { id: "CLO876", name: "Close Up", category: "Toothpaste", stock: false },
      { id: "PEA531", name: "Pears", category: "Soap", stock: true },
      { id: "LU7264", name: "Lux", category: "Soap", stock: false },
      { id: "COL112", name: "Colgate", category: "Toothpaste", stock: true },
      { id: "DM881", name: "Dairy Milk", category: "Food", stock: false },
      { id: "LI130", name: "Liril", category: "Soap", stock: true },
      { id: "PPS613", name: "Pepsodent", category: "Toothpaste", stock: false },
      { id: "MAG441", name: "Maggi", category: "Food", stock: true },
    ],
    optionsArray: {
      prodName: [
        "Pepsi",
        "Close Up",
        "Pears",
        "Lux",
        "Colgate",
        "Dairy Milk",
        "Liril",
        "Pepsodent",
        "Maggi",
      ],
    },
    optionsSel: {
      prodName: [],
    },
  };

  handleChangeOption = (OptSel) => {
    let s1 = { ...this.state };
    s1.optionsSel = OptSel;
    this.setState(s1);
  };

  showProducts = () => {
    const { products, optionsSel } = this.state;
    const { prodName } = optionsSel;
    const product1 =
      prodName.length > 0
        ? products.filter(
            (p1) => prodName.findIndex((nam) => nam === p1.name) >= 0
          )
        : products;
    return (
      <div className="container">
        <hr />
        {product1.map((p1) => {
          let { id, name, category, stock } = p1;
          let Stock = stock ? "True" : "False";
          return (
            <div className="row">
              <div className="col-3 border bg-light">{id}</div>
              <div className="col-3  border bg-light">{name}</div>
              <div className="col-3 border bg-light">{category}</div>
              <div className="col-3 border bg-light">{Stock}</div>
            </div>
          );
        })}
      </div>
    );
  };

  render() {
    const { optionsArray, optionsSel } = this.state;
    return (
      <React.Fragment>
        <div className="container">
          <div className="row">
            <div className="col-3 border bg-light">
              <LPForm1
                optionsArray={optionsArray}
                optionsSel={optionsSel}
                onChangeOption={this.handleChangeOption}
                onClear={this.handleClear}
              />
            </div>
            {optionsSel.prodName.length <= 0 ? (
              <div className="col-9 border">
                {" "}
                <h6>Select products :</h6>
              </div>
            ) : (
              <div className="col-9 border">
                <p> 
                  <b> Selected Products : </b>
                  {optionsSel.prodName.map((p1) => p1).join(", ")}
                </p>
            
                {this.showProducts()}
              </div>
            )}
          </div>
        </div>
      </React.Fragment>
    );
  }
}
export default MainProdCompo1;
