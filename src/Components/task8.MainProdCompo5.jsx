import React, { Component } from "react";
import LPForm5 from "./task8.LPForm5";
class MainProdCompo5 extends Component {
  state = {
    products: [
      { id: "PEP110", name: "Pepsi", category: "Food", stock: "yes" },
      { id: "CLO876", name: "Close Up", category: "Toothpaste", stock: "no" },
      { id: "PEA531", name: "Pears", category: "Soap", stock: "arriving" },
      { id: "LU7264", name: "Lux", category: "Soap", stock: "yes" },
      { id: "COL112", name: "Colgate", category: "Toothpaste", stock: "no" },
      { id: "DM881", name: "Dairy Milk", category: "Food", stock: "arriving" },
      { id: "LI130", name: "Liril", category: "Soap", stock: "yes" },
      { id: "PPS613", name: "Pepsodent", category: "Toothpaste", stock: "no" },
      { id: "MAG441", name: "Maggi", category: "Food", stock: "arriving" },
      { id: "PNT560", name: "Pantene", category: "Shampoo", stock: "no" },
      { id: "KK219", name: "KitKat", category: "Food", stock: "arriving" },
      { id: "DOV044", name: "Dove", category: "Soap", stock: "yes" },
    ],
    optionsArray: {
      prodCateg: ["Food", "Toothpaste", "Soap", "Shampoo"],
      stockStatus: ["yes", "no", "arriving"],
    },
    optionsSel: {
      prodCateg: [],
      stockStatus: "",
    },
  };

  handleChangeOption = (OptSel) => {
    let s1 = { ...this.state };
    s1.optionsSel = OptSel;
    this.setState(s1);
  };

  showProducts = () => {
    const { products, optionsSel } = this.state;
    const { prodCateg, stockStatus } = optionsSel;
    const product1 =
      prodCateg.length > 0
        ? products.filter(
            (p1) => prodCateg.findIndex((nam) => nam === p1.category) >= 0
          )
        : products;
    const product2 = stockStatus
      ? product1.filter((p1) => p1.stock === stockStatus)
      : product1;

    return (
      <div className="container">
        <hr />
        {product2.map((p1) => {
          let { id, name, category, stock } = p1;

          return (
            <div className="row">
              <div className="col-3 border bg-light">{id}</div>
              <div className="col-3  border bg-light">{name}</div>
              <div className="col-3 border bg-light">{category}</div>
              <div className="col-3 border bg-light">{stock}</div>
            </div>
          );
        })}
      </div>
    );
  };

  render() {
    const { optionsArray, optionsSel } = this.state;
    return (
      <React.Fragment>
        <div className="container">
          <div className="row">
            <div className="col-3 border bg-light">
              <LPForm5
                optionsArray={optionsArray}
                optionsSel={optionsSel}
                onChangeOption={this.handleChangeOption}
                onClear={this.handleClear}
              />
            </div>
            {optionsSel.prodCateg.length <= 0 ? (
              <div className="col-9 border">
                {" "}
                <h6>Selecte Category :</h6>
                <p>
                  {" "}
                  <b>Stock Status: </b>
                  {optionsSel.stockStatus ? optionsSel.stockStatus : "All"}
                </p>
              </div>
            ) : (
              <div className="col-9 border">
                <p>
                  <b> Category : </b>
                  {optionsSel.prodCateg.map((p1) => p1).join(", ")}
                </p>

                {this.showProducts()}
              </div>
            )}
          </div>
        </div>
      </React.Fragment>
    );
  }
}
export default MainProdCompo5;
