import React, { Component } from "react";

import LeftPanelFormUsingDD from "./task1.5.LeftPanelFormAddDD";
class MainComponentLaptopUpdate extends Component {
  state = {
    laptops: [
      {
        model: "AX4581",
        brand: "Acer",
        ram: "4GB",
        hardDisk: "500GB",
        rating: "3",
        processor: "i3",
      },
      {
        model: "HB2881",
        brand: "HP",
        ram: "4GB",
        hardDisk: "250GB",
        rating: "4",
        processor: "i3",
      },
      {
        model: "DM811",
        brand: "Dell",
        ram: "4GB",
        hardDisk: "1TB",
        rating: "3",
        processor: "i3",
      },
      {
        model: "AP629",
        brand: "Acer",
        ram: "6GB",
        hardDisk: "1TB",
        rating: "2",
        processor: "i3",
      },
      {
        model: "AT820",
        brand: "Acer",
        ram: "8GB",
        hardDisk: "1TB",
        rating: "4",
        processor: "i5",
      },
      {
        model: "HK008",
        brand: "HP",
        ram: "6GB",
        hardDisk: "250GB",
        rating: "3",
        processor: "i5",
      },
      {
        model: "MAir11",
        brand: "Apple",
        ram: "4GB",
        hardDisk: "128GB",
        rating: "4",
        processor: "i5",
      },
      {
        model: "MPro24X",
        brand: "Apple",
        ram: "8GB",
        hardDisk: "256GB",
        rating: "4",
        processor: "i7",
      },
      {
        model: "DL390",
        brand: "Dell",
        ram: "6GB",
        hardDisk: "500GB",
        rating: "3",
        processor: "i3",
      },
      {
        model: "AM954",
        brand: "Acer",
        ram: "8GB",
        hardDisk: "1TB",
        rating: "3",
        processor: "i7",
      },
      {
        model: "AB234",
        brand: "Acer",
        ram: "4GB",
        hardDisk: "250GB",
        rating: "2",
        processor: "i3",
      },
      {
        model: "HC874",
        brand: "HP",
        ram: "8GB",
        hardDisk: "1TB",
        rating: "1",
        processor: "i7",
      },
    ],
    optionsArray: {
      brand: ["Acer", "HP", "Dell", "Apple"],
      ram: ["4GB", "6GB", "8GB"],
      rating: ["1", "2", "3", "4"],
      processor: ["i3", "i5", "i7"],
      hardDisk: ["128GB", "250GB", "256GB", "500GB", "1TB"],
    },
    optionsSel: {
      brand: "",
      ram: "",
      processor: "",
      rating: "",
      hardDisk: [],
    },
  };

  showLaptop = () => {
    const { laptops, optionsSel } = this.state;
    const { brand, ram, processor, rating, hardDisk } = optionsSel;
    const laptop1 = brand
      ? laptops.filter((lt) => brand === lt.brand)
      : laptops;

    const laptop2 = ram ? laptop1.filter((lt) => lt.ram === ram) : laptop1;

    const laptop3 = processor
      ? laptop2.filter((lt) => lt.processor === processor)
      : laptop2;
    const laptop4 = rating
      ? laptop3.filter((lt) => lt.rating === rating)
      : laptop3;
    const laptop5 =
      hardDisk.length > 0
        ? laptops.filter(
            (lt) => hardDisk.findIndex((hd) => hd === lt.hardDisk) >= 0
          )
        : laptop4;

    return (
      <div className="container">
        <h4>Selected Laptops</h4>
        <hr />
        {laptop5.map((lap1) => {
          let { model, brand, ram, hardDisk, rating, processor } = lap1;
          return (
            <div className="row">
              <div className="col-2 border">{model}</div>
              <div className="col-2  border">{brand}</div>
              <div className="col-2 border">{ram}</div>
              <div className="col-2 border">{hardDisk}</div>
              <div className="col-2 border">{rating}</div>
              <div className="col-2 border">{processor}</div>
            </div>
          );
        })}
      </div>
    );
  };

  handleClear = () => {
    let s1 = { ...this.state };
    s1.optionsSel = {
      brand: "",
      ram: "",
      processor: "",
      rating: "",
      hardDisk: [],
    };
    this.setState(s1);
  };

  handleChangeOption = (OptSel) => {
    let s1 = { ...this.state };
    s1.optionsSel = OptSel;
    this.setState(s1);
  };

  render() {
    const { optionsArray, optionsSel } = this.state;
    return (
      <React.Fragment>
        <div className="container">
          <div className="row">
            <div className="col-3 border bg-light">
              <LeftPanelFormUsingDD
                optionsArray={optionsArray}
                optionsSel={optionsSel}
                onChangeOption={this.handleChangeOption}
                onClear={this.handleClear}
              />
            </div>
            <div className="col-9 border"> {this.showLaptop()}</div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
export default MainComponentLaptopUpdate;
