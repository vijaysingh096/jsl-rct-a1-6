import React, { Component } from "react";
import LPForm2 from "./task6.2.LPForm2";
class MainProdCompo2 extends Component {
  state = {
    products: [
      { id: "PEP110", name: "Pepsi", category: "Food", stock: true },
      { id: "CLO876", name: "Close Up", category: "Toothpaste", stock: false },
      { id: "PEA531", name: "Pears", category: "Soap", stock: true },
      { id: "LU7264", name: "Lux", category: "Soap", stock: false },
      { id: "COL112", name: "Colgate", category: "Toothpaste", stock: true },
      { id: "DM881", name: "Dairy Milk", category: "Food", stock: false },
      { id: "LI130", name: "Liril", category: "Soap", stock: true },
      { id: "PPS613", name: "Pepsodent", category: "Toothpaste", stock: false },
      { id: "MAG441", name: "Maggi", category: "Food", stock: true },
    ],
    optionsArray: {
      prodCateg: [
        "Food",
        "Toothpaste",
        "Soap",
      
      ],
    },
    optionsSel: {
      prodCateg: [],
    },
  };

  handleChangeOption = (OptSel) => {
    let s1 = { ...this.state };
    s1.optionsSel = OptSel;
    this.setState(s1);
  };

  showProducts = () => {
    const { products, optionsSel } = this.state;
    const { prodCateg } = optionsSel;
    const product1 =
      prodCateg.length > 0
        ? products.filter(
            (p1) => prodCateg.findIndex((ct1) => ct1 === p1.category) >= 0
          )
        : products;
    return (
      <div className="container">
        <hr />
        {product1.map((p1) => {
          let { id, name, category, stock } = p1;
          let Stock = stock ? "True" : "False";
          return (
            <div className="row">
              <div className="col-3 border bg-light">{id}</div>
              <div className="col-3  border bg-light">{name}</div>
              <div className="col-3 border bg-light">{category}</div>
              <div className="col-3 border bg-light">{Stock}</div>
            </div>
          );
        })}
      </div>
    );
  };

  render() {
    const { optionsArray, optionsSel } = this.state;
    return (
      <React.Fragment>
        <div className="container">
          <div className="row">
            <div className="col-3 border bg-light">
              <LPForm2
                optionsArray={optionsArray}
                optionsSel={optionsSel}
                onChangeOption={this.handleChangeOption}
                onClear={this.handleClear}
              />
            </div>
            {optionsSel.prodCateg.length <= 0 ? (
              <div className="col-9 border">
                {" "}
                <h6>Select products category :</h6>
              </div>
            ) : (
              <div className="col-9 border">
                <p>
                  <b> Selected Products category : </b>
                  {optionsSel.prodCateg.map((p1) => p1).join(", ")}
                </p>
                <hr />
                {this.showProducts()}
              </div>
            )}
          </div>
        </div>
      </React.Fragment>
    );
  }
}
export default MainProdCompo2;
