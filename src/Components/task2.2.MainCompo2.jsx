import React, { Component } from "react";

import InputCompo2 from "./task2.2.InputCompo2";
class Task2_2_MainCompo2 extends Component {
  state = {
    customers: [],
    optionArr: {
      gender: ["Male", "Female"],
      delivery: ["Office", "Pickup", "Home"],
      payment: ["Credit Card", "Debit Card", "Net Banking", "UPI"],
      delSlot: ["Before 10 AM", "10AM- 12PM", "12PM-2PM", "2PM-6PM"],
    },
    slectedOpt: {
      name: "",
      gender: "",
      delivery: "",
      payment: [],
      delSlot: "",
    },
    view: 0,
    editIndex: -1,
  };

  showForm = () => {
    let s1 = { ...this.state };
    s1.view = 1;
    this.setState(s1);
  };
  showCustomerList = () => {
    let s1 = { ...this.state };
    s1.view = 2;
    this.setState(s1);
  };

  handleSubmit = (Details) => {
    let s1 = { ...this.state };
    s1.editIndex >= 0
      ? (s1.customers[s1.editIndex] = Details)
      : s1.customers.push(Details);
      s1.view = 2;
s1.editIndex=-1
    // s1.slectedOpt = {
    //   name: "",
    //   gender: "",
    //   delivery: "",
    //   payment: [],
    //   delSlot: "",
    // };
    this.setState(s1);
  };
  handleChangeOption = (OptSel) => {
    let s1 = { ...this.state };
    s1.slectedOpt = OptSel;
    this.setState(s1);
  };

  handleEdit = (index) => {
    let s1 = { ...this.state };
    s1.view = 1;
    s1.editIndex = index;
    this.setState(s1);
  };

  render() {
    let { customers, view, optionArr, slectedOpt, editIndex } = this.state;
    return (
      <React.Fragment>
        <div className="container">
          <button className="btn btn-primary" onClick={this.showForm}>
            New Customer
          </button>{" "}
          <button className="btn btn-primary" onClick={this.showCustomerList}>
            List Of Customer
          </button>{" "}
          {view === 0 ? (
            <h6> Welcome to the Customer System </h6>
          ) : view === 1 ? (
            <InputCompo2
              customer={editIndex >= 0 ? customers[editIndex] : slectedOpt}
              optionArr={optionArr}
              onChangeOption={this.handleChangeOption}
              onSubmit={this.handleSubmit}
            />
          ) : customers.length === 0 ? (
            <h5> Ther are ZERO Customer </h5>
          ) : (
            <>
              <div className="row border bg-dark text-white">
                <div className="col-2 border">Name</div>
                <div className="col-2  border">Gender</div>
                <div className="col-2 border">Delivery</div>
                <div className="col-2 border">Payment</div>
                <div className="col-2 border">Slot</div>
                <div className="col-2 border"></div>
              </div>

              {customers.map((c1, index) => {
                let { name, gender, delivery, payment = [], delSlot } = c1;
                return (
                  <div className="row">
                    <div className="col-2 border">{name}</div>
                    <div className="col-2  border">{gender}</div>
                    <div className="col-2 border">{delivery}</div>
                    <div className="col-2 border">
                      {payment.map((m1) => m1).join(",")}
                    </div>
                    <div className="col-2 border">{delSlot}</div>
                    <div className="col-2 border">
                      <button
                        className="btn alert-success"
                        onClick={this.handleEdit(index)}
                      >
                        Edit
                      </button>{" "}
                    </div>
                  </div>
                );
              })}
            </>
          )}
        </div>
      </React.Fragment>
    );
  }
}
export default Task2_2_MainCompo2;
