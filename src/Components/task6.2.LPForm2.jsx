import React, { Component } from "react";
class LPForm2 extends Component {
  state = {};
  handleChange = (e) => {
    let s1 = { ...this.props.optionsSel };
    let { currentTarget: input } = e;
    input.name === "prodCateg"
      ? (s1.prodCateg = this.updateCBs(input.checked, input.value, s1.prodCateg))
      : (s1[input.name] = input.value);
    this.props.onChangeOption(s1);
  };

  updateCBs = (checked, val, arr) => {
    if (checked) arr.push(val);
    else {
      let index = arr.findIndex((v1) => v1 === val);
      if (index >= 0) arr.splice(index, 1);
    }
    return arr;
  };
  render() {
    let { optionsArray, optionsSel } = this.props;
    return (
      <React.Fragment>
        <div className="container">
          {this.showCheckBoxes(
            "Product Name",
            optionsArray.prodCateg,
            "prodCateg",
            optionsSel.prodCateg
          )}
        </div>
      </React.Fragment>
    );
  }

  showCheckBoxes = (label, arr, name, selArr) => {
    return (
      <React.Fragment>
        <label className="form-check-lable">
          <b>{label}</b>
        </label>
        {arr.map((opt) => (
          <div className="form-check">
            <input
              className="form-check-input"
              type="checkbox"
              name={name}
              value={opt}
              checked={selArr.findIndex((sel) => sel === opt) >= 0}
              onChange={this.handleChange}
            />
            <label className="form-check-lable">{opt}</label>
          </div>
        ))}
        <br />
      </React.Fragment>
    );
  };
}
export default LPForm2;
