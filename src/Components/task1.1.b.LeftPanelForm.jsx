import React, { Component } from "react";
class LeftPanelForm extends Component {
  handleChange = (e) => {
    let s1 = { ...this.props.optionsSel };
    let { currentTarget: input } = e;
    input.name === "brand"
      ? (s1.brand = this.updateCBs(input.checked, input.value, s1.brand))
      : input.name === "ram"
      ? (s1.ram = this.updateCBs(input.checked, input.value, s1.ram))
      : (s1[input.name] = input.value);
    this.props.onChangeOption(s1);
  };

  updateCBs = (checked, val, arr) => {
    if (checked) arr.push(val);
    else {
      let index = arr.findIndex((v1) => v1 === val);
      if (index >= 0) arr.splice(index, 1);
    }
    return arr;
  };
 
  render() {
    let { optionsArray, optionsSel } = this.props;
    return (
      <React.Fragment>
        <div className="container">
          <h5>Choose Options</h5>
          <hr />
          <button
            className="btn btn-warning btn-sm"
            onClick={this.props.onClear}
          >
            {" "}
            Clear All
          </button>{" "}
          <br />
          {this.showCheckBoxes(
            "Brand",
            optionsArray.brand,
            "brand",
            optionsSel.brand
          )}
          {this.showCheckBoxes("RAM", optionsArray.ram, "ram", optionsSel.ram)}
          {this.showRadioes(
            "Processor",
            optionsArray.processor,
            "processor",
            optionsSel.processor
          )}
          {this.showRadioes(
            "Rating",
            optionsArray.rating,
            "rating",
            optionsSel.rating
          )}{" "}
        </div>
      </React.Fragment>
    );
  }
  showCheckBoxes = (label, arr, name, selArr) => {
    return (
      <React.Fragment>
        <label className="form-check-lable">
          <b>{label}</b>
        </label> 
        {arr.map((opt) => (
          <div className="form-check">
            <input
              className="form-check-input"
              type="checkbox"
              name={name}
              value={opt}
              checked={selArr.findIndex((sel) => sel === opt) >= 0}
              onChange={this.handleChange}
            />
            <label className="form-check-lable">{opt}</label>
          </div>
        ))}
        <br />
      </React.Fragment>
    );
  };

  showRadioes = (label, arr, name, selVal) => {
    return (
      <React.Fragment>
        <label className="form-check-lable font-weight-bold">
          <b>{label} </b>
        </label>
        <br />
        {arr.map((r1) => (
          <div className="form-check ">
            <input
              className="form-check-input"
              type="radio"
              name={name}
              value={r1}
              checked={selVal === r1}
              onChange={this.handleChange}
            />
            <label className="form-check-lable">{r1}</label>
          </div>
        ))}
        <br />
      </React.Fragment>
    );
  };
}
export default LeftPanelForm;
