import React, { Component } from "react";
class InputCompo2 extends Component {
  handleChange = (e) => {
    console.log(this.props.customer);
    let s1 = { ...this.props.customer };
    let { currentTarget: input } = e;
    input.name === "payment"
      ? (s1.payment = this.updateCBs(input.checked, input.value, s1.payment))
      : (s1[input.name] = input.value);
    this.props.onChangeOption(s1);
  };

  handleSubmit = (e) => {
    e.preventDefault();
    console.log(this.props.customer);
    this.props.onSubmit(this.props.customer);
  };

  updateCBs = (checked, val, arr) => {
    if (checked) arr.push(val);
    else {
      let index = arr.findIndex((v1) => v1 === val);
      if (index >= 0) arr.splice(index, 1);
    }
    return arr;
  };
  render() {
    let { customer, optionArr } = this.props;
    let { gender, delivery, payment, delSlot } = optionArr;
    return (
      <React.Fragment>
        <div className="container">
          {this.showTextField("Name", "text", "name", "name", customer.name)}
          {this.showRadioes("Gender", gender, "gender", customer.gender)}
          {this.showRadioes(
            "Choose your Delivery Option",
            delivery,
            "delivery",
            customer.delivery
          )}
          {this.showCheckBoxes(
            "Choose your Payment Option",
            payment,
            "payment",
            customer.payment
          )}
          {this.showDD(
            "Delivery Slot",
            "slotOpt",
            "delSlot",
            "Select the delivery Slot",
            delSlot,
            customer.delSlot
          )}
          <button className="btn btn-primary my-2" onClick={this.handleSubmit}>
            Submit
          </button>{" "}
        </div>
      </React.Fragment>
    );
  }

  showTextField = (lable, type, id, name, val) => {
    return (
      <div className="form-group">
        <label>{lable}</label>
        <input
          type={type}
          className="form-control"
          id={id}
          name={name}
          value={val}
          placeholder="Enter  Name "
          onChange={this.handleChange}
        />
      </div>
    );
  };
  showCheckBoxes = (label, arr, name, selArr) => {
    return (
      <React.Fragment>
        <label className="form-check-lable">
          <b>{label}</b>
        </label>
        {arr.map((opt) => (
          <div className="form-check">
            <input
              className="form-check-input"
              type="checkbox"
              name={name}
              value={opt}
              checked={selArr.findIndex((sel) => sel === opt) >= 0}
              onChange={this.handleChange}
            />
            <label className="form-check-lable">{opt}</label>
          </div>
        ))}
      </React.Fragment>
    );
  };
  showDD = (label, id, name, firstOpt, arr, selVal) => {
    return (
      <React.Fragment>
        <div className="form-group font-weight-bold">
          <label>
            <b> {label} </b>
          </label>
          <select
            className="form-control"
            id={id}
            name={name}
            value={selVal ? selVal : ""}
            onChange={this.handleChange}
          >
            <option disabled value="">
              {firstOpt}
            </option>
            {arr.map((opt1) => {
              return <option>{opt1}</option>;
            })}
          </select>
        </div>
      </React.Fragment>
    );
  };

  showRadioes = (label, arr, name, selVal) => {
    return (
      <React.Fragment>
        <label className="form-check-lable ">
          <b>{label} </b>
        </label>
        <br />
        {arr.map((r1) => (
          <div className="form-check form-check-inline">
            <input
              className="form-check-input"
              type="radio"
              name={name}
              value={r1}
              checked={selVal === r1}
              onChange={this.handleChange}
            />
            <label className="form-check-lable">{r1}</label>
          </div>
        ))}
        <br />
      </React.Fragment>
    );
  };
}
export default InputCompo2;
