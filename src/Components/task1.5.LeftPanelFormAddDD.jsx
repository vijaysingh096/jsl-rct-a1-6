import React, { Component } from "react";
class LeftPanelFormUsingDD extends Component {
  handleChange = (e) => {
    let s1 = { ...this.props.optionsSel };
    let { currentTarget: input } = e;
    input.name === "hardDisk"
      ? (s1.hardDisk = this.updateCBs(input.checked, input.value, s1.hardDisk))
      : (s1[input.name] = input.value);
    this.props.onChangeOption(s1);
  };

  updateCBs = (checked, val, arr) => {
    if (checked) arr.push(val);
    else {
      let index = arr.findIndex((v1) => v1 === val);
      if (index >= 0) arr.splice(index, 1);
    }
    return arr;
  };

  render() {
    let { optionsArray, optionsSel } = this.props;
    return (
      <React.Fragment>
        <div className="container">
          <h5>Choose Options</h5>
          <hr />
          <button
            className="btn btn-warning btn-sm"
            onClick={this.props.onClear}
          >
            {" "}
            Clear All
          </button>{" "}
          <br />
          {this.showDD(
            "Brand",
            "brand",
            "Select brand",
            optionsArray.brand,
            optionsSel.brand
          )}
          {this.showDD(
            "RAM",
            "ram",
            "Select RAM",
            optionsArray.ram,
            optionsSel.ram
          )}
          {this.showRadioes(
            "Processor",
            optionsArray.processor,
            "processor",
            optionsSel.processor
          )}
          {this.showRadioes(
            "Rating",
            optionsArray.rating,
            "rating",
            optionsSel.rating
          )}{" "}
          {this.showCheckBoxes(
            "Hard Disk",
            optionsArray.hardDisk,
            "hardDisk",
            optionsSel.hardDisk
          )}{" "}
        </div>
      </React.Fragment>
    );
  }
  showCheckBoxes = (label, arr, name, selArr) => {
    return (
      <React.Fragment>
        <label className="form-check-lable">
          <b>{label}</b>
        </label>
        {arr.map((opt) => (
          <div className="form-check">
            <input
              className="form-check-input"
              type="checkbox"
              name={name}
              value={opt}
              checked={selArr.findIndex((sel) => sel === opt) >= 0}
              onChange={this.handleChange}
            />
            <label className="form-check-lable">{opt}</label>
          </div>
        ))}
        <br />
      </React.Fragment>
    );
  };

  showDD = (label, name, firstOpt, arr, selVal) => {
    return (
      <React.Fragment>
        <div className="form-group font-weight-bold">
          <label>
            <b> {label} </b>
          </label>
          <select
            className="form-control"
            id="workExp"
            name={name}
            value={selVal ? selVal : ""}
            onChange={this.handleChange}
          >
            <option disabled value="">
              {firstOpt}
            </option>
            {arr.map((opt1) => {
              return <option>{opt1}</option>;
            })}
          </select>
        </div>
      </React.Fragment>
    );
  };

  showRadioes = (label, arr, name, selVal) => {
    return (
      <React.Fragment>
        <label className="form-check-lable font-weight-bold">
          <b>{label} </b>
        </label>
        <br />
        {arr.map((r1) => (
          <div className="form-check ">
            <input
              className="form-check-input"
              type="radio"
              name={name}
              value={r1}
              checked={selVal === r1}
              onChange={this.handleChange}
            />
            <label className="form-check-lable">{r1}</label>
          </div>
        ))}
        <br />
      </React.Fragment>
    );
  };
}
export default LeftPanelFormUsingDD;
