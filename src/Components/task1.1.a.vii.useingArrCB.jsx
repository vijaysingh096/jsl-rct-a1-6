import React, { Component } from "react";
class CB_usingArr extends Component {
  state = {
    person: {
      name: "",
      age: "",
      country: "",
      gender: "",
      passport: "",
      license: "",
      city: "",
      passportNo: "",
      designation: "",
      techsKnown: [],
    },
    countries: ["United State of America", "Canada", "India", "United Kingdom"],
    countryList: [
      {
        country: "United State of America",
        cities: ["New Yark", "Los Angeles", "Seattle", "San Franceisco"],
      },
      {
        country: "Canada",
        cities: ["Toronto", "Montreal", "Vancouver"],
      },
      {
        country: "India",
        cities: ["New Delhi", "Bengaluru", "Pune", "Chennai"],
      },
      {
        country: "United Kingdom",
        cities: ["London", "Bristol", "Manchester"],
      },
    ],
    designations: [
      "Developer",
      "Senior Developer",
      "Team Lead",
      "Architect",
      "Delivery Manager",
    ],
    techs: ["React", "Javascript", "JQuery", "AngularJS"],
  };
  handleChange = (e) => {
    let { currentTarget: input } = e;
    let s1 = { ...this.state };
    input.type === "checkbox"
      ? input.name === "techsKnown"
        ? (s1.person.techsKnown = this.updateCBs(
            input.checked,
            input.value,
            s1.person.techsKnown
          ))
        : (s1.person[input.name] = input.checked)
      : (s1.person[input.name] = input.value);
    if (input.name === "country") s1.person.city = "";
    if (!s1.person.passport) s1.person.passportNo = "";
    this.setState(s1);
  };

  updateCBs = (checkStatus, val, arr) => {
    if (checkStatus) arr.push(val);
    else {
      let index = arr.findIndex((ele) => ele === val);
      if (index >= 0) arr.splice(index, 1);
    }
    console.log(arr);
    return arr;
  };

  handelSubmit = (e) => {
    e.preventDefault();
  };
  render() {
    let { person, countries, countryList, designations, techs } = this.state;
    let {
      name,
      age,
      country,
      gender,
      passport,
      license,
      city,
      passportNo,
      designation,
      techsKnown,
    } = person;
    const cities = country
      ? countryList.find((c1) => c1.country === country).cities
      : [];
    return (
      <React.Fragment>
        <div className="container">
          <div className="form-group">
            <label>Name</label>
            <input
              type="text"
              className="form-control"
              id="name"
              name="name"
              value={name}
              placeholder="Enter  Name "
              onChange={this.handleChange}
            />
          </div>

          <div className="form-group"> 
            <label>Age</label>
            <input
              type="text"
              className="form-control"
              id="age"
              name="age"
              value={age}
              placeholder="Enter  Age "
              onChange={this.handleChange}
            />
          </div>
          <div className="form-group">
            <label>Country</label>
            <select
              className="form-control"
              id="country"
              name="country"
              value={country}
              onChange={this.handleChange}
            >
              <option disabled value="">
                select the country
              </option>
              {countries.map((c1) => {
                return <option>{c1}</option>;
              })}
            </select>
          </div>
          {country ? (
            <div className="form-group">
              <label>City</label>
              <select
                className="form-control"
                id="city"
                name="city"
                value={city}
                onChange={this.handleChange}
              >
                <option disabled value="">
                  select the city
                </option>
                {cities.map((c1) => {
                  return <option>{c1}</option>;
                })}
              </select>
            </div>
          ) : (
            ""
          )}
          <div className="form-check form-check-inline">
            <input
              className="form-check-input"
              type="radio"
              name="gender"
              value="Male"
              checked={gender === "Male"}
              onChange={this.handleChange}
            />
            <label className="form-check-lable">Male</label>
          </div>
          <div className="form-check form-check-inline">
            <input
              className="form-check-input"
              type="radio"
              name="gender"
              value="Female"
              checked={gender === "Female"}
              onChange={this.handleChange}
            />
            <label className="form-check-lable">Female</label>
          </div>
          <br />

          <div className="form-check">
            <input
              className="form-check-input"
              type="checkbox"
              name="passport"
              value="passport"
              checked={passport}
              onChange={this.handleChange}
            />
            <label className="form-check-lable">Passport</label>
          </div>
          {passport ? (
            <div className="form-group">
              <label>Enter the Passport Number </label>
              <input
                type="text"
                className="form-control"
                id="passportNo"
                name="passportNo"
                value={passportNo}
                placeholder="Enter  Passport Number "
                onChange={this.handleChange}
              />
            </div>
          ) : (
            ""
          )}
          <div className="form-check">
            <input
              className="form-check-input"
              type="checkbox"
              name="license"
              value="license"
              checked={license}
              onChange={this.handleChange}
            />
            <label className="form-check-lable">License</label>
          </div>

          <label className="form-check-lable font-weight-bold">
            <b> Choose the Designation</b>
          </label>
          <br />
          {designations.map((d1) => (
            <div className="form-check form-check-inline">
              <input
                className="form-check-input"
                type="radio"
                name="designation"
                value={d1}
                checked={designation === d1}
                onChange={this.handleChange}
              />
              <label className="form-check-lable">{d1}</label>
            </div>
          ))}
          <br />
          <label className="form-check-lable">
            <b>Choose Technologys</b>
          </label>
          {techs.map((t1) => (
            <div className="form-check">
              <input
                className="form-check-input"
                type="checkbox"
                name="techsKnown"
                value={t1}
                checked={techsKnown.findIndex((tech) => tech === t1) >= 0}
                onChange={this.handleChange}
              />
              <label className="form-check-lable">{t1}</label>
            </div>
          ))}
          <button className="btn btn-primary my-2" onClick={this.handelSubmit}>
            Submit
          </button>
        </div>
      </React.Fragment>
    );
  }
}
export default CB_usingArr;
