import React, { Component } from "react";
import FormComponent from "./task1.1.a.viii.FormComponent";
class MainComponenet1a extends Component {
  state = {
    person: [
      {
        name: "Williams",
        age: 27,
        country: "",
        gender: "Male",
        passport: true,
        license: "",
        city: "",
        passportNo: "FGRRT54234",
        designation: "",
        techsKnown: [],
      },
      {
        name: "Anna",
        age: 31,
        country: "",
        gender: "Female",
        passport: false,
        license: "",
        city: "",
        passportNo: "",
        designation: "",
        techsKnown: [],
      },
    ],
    view: 0,
    editPersonIndex: -1,
  };

  handleSubmit = (Person) => {
    console.log(Person);
    let s1 = { ...this.state };
    s1.editPersonIndex >= 0
      ? (s1.person[s1.editPersonIndex] = Person)
      : s1.person.push(Person);
    s1.view = 0;
    s1.editPersonIndex = -1;
    this.setState(s1);
  };

  showForm = () => {
    let s1 = { ...this.state };
    s1.view = 1;
    s1.editPersonIndex = -1;
    this.setState(s1);
  };

  deletePerson = (index) => {
    let s1 = { ...this.state };
    s1.person.splice(index, 1);
    s1.view = 0;
    this.setState(s1);
  };
  editPerson = (index) => {
    let s1 = { ...this.state };
    s1.view = 1;
    s1.editPersonIndex = index;
    this.setState(s1);
  };
  render() {
    let { person, view, editPersonIndex } = this.state;
    let Person1 = {
      name: "",
      age: "",
      country: "",
      gender: "",
      passport: "",
      license: "",
      city: "",
      passportNo: "",
      designation: "",
      techsKnown: [],
    };
    return (
      <React.Fragment>
        <div className="container">
          {view === 0 ? (
            <>
              <h4> Details of persons</h4>
              {person.map((p1, index) => {
                return (
                  <div className="row border">
                    <div className="col-4 border">{p1.name} </div>
                    <div className="col-4 border">{p1.age} </div>
                    <div className="col-4 border">
                      <button
                        className="btn btn-danger btn-sm m-2"
                        onClick={() => this.deletePerson(index)}
                      >
                        Delete
                      </button>
                      <button
                        className="btn btn-warning btn-sm m-2"
                        onClick={() => this.editPerson(index)}
                      >
                        Edit
                      </button>
                    </div>
                  </div>
                );
              })}
              <button
                className="btn btn-primary  my-2"
                onClick={() => this.showForm()}
              >
                Add person
              </button>
            </>
          ) : (
            <FormComponent
              person={editPersonIndex >= 0 ? person[editPersonIndex] : Person1}
              onSubmit={this.handleSubmit}
              edit={editPersonIndex >= 0}
            />
          )}
        </div>
      </React.Fragment>
    );
  }
}
export default MainComponenet1a;
