import React, { Component } from "react";
class InputFieldUsingRB extends Component {
  state = {
    selcVal: {
      status: "",
      company: "",
      designation: "",
      college: "",
      course: "",
    },
  };

  handleChange = (e) => {
    let s1 = { ...this.state };
    let { currentTarget: input } = e;

    s1.selcVal[input.name] = input.value;
    if (!s1.selcVal.status === "working") {
      s1.selcVal.company = "";
      s1.selcVal.designation = "";
    }
    if (!s1.selcVal.status === "studying") {
      s1.selcVal.college = "";
      s1.selcVal.course = "";
    }
    this.setState(s1);
    // console.log(this.state);
  };

  handelSubmit = (e) => {
    e.preventDefault();
    let { status, company, designation, college, course } = this.state.selcVal;
    console.log(this.state.selcVal);
    let wAlert =
      status === "working"
        ? status + ", Company- " + company + ", Designation- " + designation
        : status === "studying"
        ? status + ", College- " + college + ", Course- " + course
        : "Select Option";

    window.alert(wAlert);
  };

  showTextField = (lable, type, id, name, val, placeholder) => {
    return (
      <div className="form-group">
        <label>{lable}</label>
        <input
          type={type}
          className="form-control"
          id={id}
          name={name}
          value={val}
          placeholder={placeholder}
          onChange={this.handleChange}
        />
      </div>
    );
  };

  render() {
    let { status, company, designation, college, course } = this.state.selcVal;
    // console.log(this.state);
    return (
      <React.Fragment>
        <div className="container">
          <h3>Provide Details below</h3>
          <div className="form-check">
            <input
              className="form-check-input"
              type="radio"
              name="status"
              value="working"
              checked={status === "working"}
              onChange={this.handleChange}
            />
            <label className="form-check-lable">Working</label>
          </div>
          {status === "working" ? (
            <>
              <h4>Provide Job Details</h4>

              {this.showTextField(
                "Company Name",
                "text",
                "compName",
                "company",
                company,
                "Enter Company Name"
              )}

              {this.showTextField(
                "Designation",
                "text",
                "desig",
                "designation",
                designation,
                "Enter Designation"
              )}
            </>
          ) : (
            ""
          )}
          <div className="form-check">
            <input
              className="form-check-input"
              type="radio"
              name="status"
              value="studying"
              checked={status === "studying"}
              onChange={this.handleChange}
            />
            <label className="form-check-lable">Studying</label>
          </div>
          {status === "studying" ? (
            <>
              <h4>Provide Cource Details</h4>

              {this.showTextField(
                "College Name",
                "text",
                "collegeName",
                "college",
                college,
                "Enter College Name"
              )}
              {this.showTextField(
                "Course",
                "text",
                "course",
                "course",
                course,
                "Enter Course name"
              )}
            </>
          ) : (
            ""
          )}
          <button className="btn btn-primary my-2" onClick={this.handelSubmit}>
            Submit
          </button>
        </div>
      </React.Fragment>
    );
  }
}
export default InputFieldUsingRB;
