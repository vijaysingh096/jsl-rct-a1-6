import React, { Component } from "react";
import LPForm4 from "./task7.2.LPForm4";
class MainProdCompo4 extends Component {
  state = {
    products: [
      { id: "PEP110", name: "Pepsi", category: "Food", stock: true },
      { id: "CLO876", name: "Close Up", category: "Toothpaste", stock: false },
      { id: "PEA531", name: "Pears", category: "Soap", stock: true },
      { id: "LU7264", name: "Lux", category: "Soap", stock: false },
      { id: "COL112", name: "Colgate", category: "Toothpaste", stock: true },
      { id: "DM881", name: "Dairy Milk", category: "Food", stock: false },
      { id: "LI130", name: "Liril", category: "Soap", stock: true },
      { id: "PPS613", name: "Pepsodent", category: "Toothpaste", stock: false },
      { id: "MAG441", name: "Maggi", category: "Food", stock: true },
    ],
    optionsArray: {
      prodCateg: [
        "Food",
        "Toothpaste",
        "Soap",
      ],
    },
    optionsSel: {
      prodCateg: "",
    },
  };

  handleChangeOption = (OptSel) => {
    let s1 = { ...this.state };
    s1.optionsSel = OptSel;
    this.setState(s1);
  };

  showProducts = () => {
    const { products, optionsSel } = this.state;
    const { prodCateg } = optionsSel;
    const product1 =
      prodCateg.length > 0
        ? products.filter((p1) => p1.category === prodCateg)                         
        : products;
    return (
      <div className="container">
        <hr />
        {product1.map((p1) => {
          let { id, name, category, stock } = p1;
          let Stock = stock ? "True" : "False";
          return (
            <div className="row">
              <div className="col-3 border bg-light">{id}</div>
              <div className="col-3  border bg-light">{name}</div>
              <div className="col-3 border bg-light">{category}</div>
              <div className="col-3 border bg-light">{Stock}</div>
            </div>
          );
        })}
      </div>
    );
  };

  render() {
    const { optionsArray, optionsSel } = this.state;
    return (
      <React.Fragment>
        <div className="container">
          <div className="row">
            <div className="col-3 border bg-light">
              <LPForm4
                optionsArray={optionsArray}
                optionsSel={optionsSel}
                onChangeOption={this.handleChangeOption}
                onClear={this.handleClear}
              />
            </div>
            {!optionsSel.prodCateg ? (
              <div className="col-9 border">
                {" "}
                <h6>Select products :</h6>
              </div>
            ) : (
              <div className="col-9 border">
                <p>
                  <b> Selected Products : </b>
                  {optionsSel.prodCateg}
                </p>

                {this.showProducts()}
              </div>
            )}
          </div>
        </div>
      </React.Fragment>
    );
  }
}
export default MainProdCompo4;
