import React, { Component } from "react";
import InputCompo from "./task2.1.InputCompo";
class Task2_1_MainCompo extends Component {
  state = {
    customers: [],
    optionArr: {
      gender: ["Male", "Female"],
      delivery: ["Office", "Pickup", "Home"],
      payment: ["Credit Card", "Debit Card", "Net Banking", "UPI"],
      delSlot: ["Before 10 AM", "10AM- 12PM", "12PM-2PM", "2PM-6PM"],
    },
    slectedOpt: {
      name: "",
      gender: "",
      delivery: "",
      payment: [],
      delSlot: "",
    },
    view: 0,
  };

  showForm = () => {
    let s1 = { ...this.state };
    s1.view = 1;
    this.setState(s1);
  };
  showCustomerList = () => {
    let s1 = { ...this.state };
    s1.view = 2;
    this.setState(s1);
  };
  handleSubmit = (Details) => {
    let s1 = { ...this.state };
    s1.customers.push(Details);
    s1.view = 2;
    s1.slectedOpt={
        name: "",
        gender: "",
        delivery: "",
        payment: [],
        delSlot: "",
    }
    this.setState(s1);
  };

  render() {
    let { customers, view, optionArr, slectedOpt } = this.state;
    return (
      <React.Fragment>
        <div className="container">
          <button className="btn btn-primary" onClick={this.showForm}>
            New Customer
          </button>{" "}
          <button className="btn btn-primary" onClick={this.showCustomerList}>
            List Of Customer
          </button>{" "}
          {view === 0 ? (
            <h6> Welcome to the Customer System </h6>
          ) : view === 1 ? (
            <InputCompo
              customer={slectedOpt}
              optionArr={optionArr}
              onSubmit={this.handleSubmit}
            />
          ) : customers.length === 0 ? (
            <h5> Ther are ZERO Customer </h5>
          ) : (
            <>
              <div className="row border bg-dark text-white">
                <div className="col-2 border">Name</div>
                <div className="col-2  border">Gender</div>
                <div className="col-2 border">Delivery</div>
                <div className="col-2 border">Payment</div>
                <div className="col-2 border">Slot</div>
                {/* <div className="col-2 border"></div> */}
              </div>

              {customers.map((c1) => {
                let { name, gender, delivery, payment = [], delSlot } = c1;
                return (
                  <div className="row">
                    <div className="col-2 border">{name}</div>
                    <div className="col-2  border">{gender}</div>
                    <div className="col-2 border">{delivery}</div>
                    <div className="col-2 border">
                      {payment.map((m1) => m1).join(",")}
                    </div>
                    <div className="col-2 border">{delSlot}</div>
                    {/* <div className="col-2 border"></div> */}
                  </div>
                );
              })}
            </>
          )}
        </div>
      </React.Fragment>
    );
  }
}
export default Task2_1_MainCompo;
