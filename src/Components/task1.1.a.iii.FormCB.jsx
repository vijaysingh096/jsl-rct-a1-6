import React, { Component } from "react";
class Task1_aFormCB extends Component {
  state = {
    person: {
      name: "",
      age: "",
      country: "",
      gender: "",
      passport: "",
      license:"",
    },
    countries: ["United State of America", "Canada", "India", "United Kingdom"],
  };
  handleChange = (e) => {
    let { currentTarget: input } = e;
    let s1 = { ...this.state };
    input.type === "checkbox"
      ? (s1.person[input.name] = input.checked)
      : (s1.person[input.name] = input.value);
    this.setState(s1);
  };
  handelSubmit = (e) => {
    e.preventDefault();
  };
  render() {
    let { person, countries } = this.state;
    let { name, age, country, gender, passport,license } = person;
    return (
      <React.Fragment>
        <div className="container">
          <div className="form-group">
            <label>Name</label>
            <input
              type="text"
              className="form-control"
              id="name"
              name="name"
              value={name}
              placeholder="Enter  Name "
              onChange={this.handleChange}
            />
          </div>

          <div className="form-group">
            <label>Age</label>
            <input
              type="text"
              className="form-control"
              id="age"
              name="age"
              value={age}
              placeholder="Enter  Age "
              onChange={this.handleChange}
            />
          </div>
          <div className="form-group">
            <label>Country</label>
            <select
              className="form-control"
              id="country"
              name="country"
              value={country}
              onChange={this.handleChange}
            >
              <option disabled value="">
                select the country
              </option>
              {countries.map((c1) => {
                return <option>{c1}</option>;
              })}
            </select>
          </div>

          <div className="form-check form-check-inline">
            <input
              className="form-check-input"
              type="radio"
              name="gender"
              value="Male"
              checked={gender === "Male"}
              onChange={this.handleChange}
            />
            <label className="form-check-lable">Male</label>
          </div>
          <div className="form-check form-check-inline">
            <input
              className="form-check-input"
              type="radio"
              name="gender"
              value="Female"
              checked={gender === "Female"}
              onChange={this.handleChange}
            />
            <label className="form-check-lable">Female</label>
          </div>
          <br />

          <div className="form-check">
            <input
              className="form-check-input"
              type="checkbox"
              name="passport"
              value="passport"
              checked={passport}
              onChange={this.handleChange}
            />
            <label className="form-check-lable">Passport</label>
          </div>

          <div className="form-check">
            <input
              className="form-check-input"
              type="checkbox"
              name="license"
              value="license"
              checked={license}
              onChange={this.handleChange}
            />
            <label className="form-check-lable">License</label>
          </div>

          <button className="btn btn-primary my-2" onClick={this.handelSubmit}>
            Submit
          </button>
        </div>
      </React.Fragment>
    );
  }
}
export default Task1_aFormCB;
