import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import Task1_aForm from "./Components/task1.1.a.i.FormDD";
import Task1_aFormRadio from "./Components/task1.1.a.ii.FormRadio";
import Task1_aFormCB from "./Components/task1.1.a.iii.FormCB";
import Task1_aFilterDD from "./Components/task1.1.a.iv.FormDDfilterDD";
import CBshowInput from "./Components/task1.1.a.v.CBshowInput";
import RB_usingArr from "./Components/task1.1.a.vi.useingArrRB";
import CB_usingArr from "./Components/task1.1.a.vii.useingArrCB";
import MainComponenet1a from "./Components/task1.1.a.viii.MainComponent";
import MainComponentLaptop from "./Components/task1.1.b.MainComponent";
import Task1b_SimpleForm from "./Components/task1.2.SimpleForm";
import SimpleFormAddRB from "./Components/task1.3.SimpleFormAddRB";
import SimpleFormAddCB from "./Components/task1.4.simpleFormAddCB";
import MainComponentLaptopUpdate from "./Components/task1.5.MainComponentUpdate";
import Task2_1_MainCompo from "./Components/task2.1.MainCompo";
import Task2_2_MainCompo2 from "./Components/task2.2.MainCompo2";
import CityAndCountryDD from "./Components/task3.CityAndCountryDD";
import InputFieldUsingCB from "./Components/task4.1FillingFormUsingCB";
import InputFieldUsingRB from "./Components/task5.1FillingFormUsingRB";
import MainProdCompo1 from "./Components/task6.1.MainProdCompo";
import MainProdCompo2 from "./Components/task6.2.MainProdCompo2";
import MainProdCompo3 from "./Components/task7.1.MainProdCompo3";
import MainProdCompo4 from "./Components/task7.2.MainProdCompo4";
import MainProdCompo5 from "./Components/task8.MainProdCompo5";

ReactDOM.render(
  <React.StrictMode>
    {/* <App /> */}
    {/* <Task1_aForm/> */}
    {/* <Task1_aFormRadio /> */}
    {/* <Task1_aFormCB /> */}
    {/* <Task1_aFilterDD /> */}
    {/* <CBshowInput /> */}
    {/* <RB_usingArr /> */}
    {/* <CB_usingArr /> */}
    {/* <MainComponenet1a/> */}
    {/* <MainComponentLaptop /> */}
    {/* <Task1b_SimpleForm /> */}
    {/* <SimpleFormAddRB /> */}
    {/* <SimpleFormAddCB /> */}
    {/* <MainComponentLaptopUpdate /> */}
    {/* <Task2_1_MainCompo /> */}
    {/* <Task2_2_MainCompo2 /> */}
    {/* <CityAndCountryDD /> */}
    {/* <InputFieldUsingCB /> */}
    {/* <InputFieldUsingRB /> */}
    {/* <MainProdCompo1 /> */}
    {/* <MainProdCompo2 /> */}
    {/* <MainProdCompo3 /> */}
    {/* <MainProdCompo4 /> */}
    <MainProdCompo5 />
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
